import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './stocks/components/home/home.component';
import { routes } from './app-routing'
import { SpinnerComponent } from './stocks/components/spinner/spinner.component';
import { StockTableComponent } from './stocks/components/stock-table/stock-table.component';
import { EffectsModule } from '@ngrx/effects';
import { StockQueryEffects } from './stocks/store/effects/stock-query.effects';
import { StoreModule } from '@ngrx/store';
import { StockQueryRequest } from './stocks/service/stock-query-requests';
import { DataPersistence } from '@nrwl/nx';
import {
  stockQueryReducer,
  STOCKQUERY_FEATURE_KEY
} from '../app/stocks/store/reducers/stock-query.reducer';
import { HttpClientModule } from '@angular/common/http';
import { MatPaginatorModule, MatProgressSpinnerModule, MatTableModule, MatSortModule, MatToolbarModule, MatToolbar } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SpinnerComponent,
    StockTableComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatButtonModule,
    HttpClientModule,
    MatTableModule,
    MatIconModule,
    MatProgressSpinnerModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot({}), 
    StoreModule.forFeature(STOCKQUERY_FEATURE_KEY, stockQueryReducer),
    EffectsModule.forRoot([StockQueryEffects])
  ],
  providers: [StockQueryRequest, DataPersistence],
  bootstrap: [AppComponent]
})
export class AppModule { }
