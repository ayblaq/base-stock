export interface StockQuery {
    symbol: string;
    name: string;
    price: number;
    change: number;
    newprice: number;
    percent: number;
    changePercent: number;
};

export interface StockQueryResponse {
    symbol: string;
    name: string;
    price: number;
};