import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { StockQuery } from '../../interface/stock-query.type';

@Component({
  selector: 'app-stock-table',
  templateUrl: './stock-table.component.html',
  styleUrls: ['./stock-table.component.css']
})
export class StockTableComponent implements OnInit {

  dataSource: MatTableDataSource<StockQuery>;
  sort: MatSort;
  paginator: MatPaginator;

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
  }

  @ViewChild(MatPaginator) set matPaginator(pr: MatPaginator) {
    this.paginator = pr;
  }

  @Input() set tableData(value: StockQuery[]) {
    this.dataSource = new MatTableDataSource(value);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  };

  @Input() date: string;
  @Input() curDay: number;

  cols: string[];
  constructor() { }

  ngOnInit() {
    this.cols = ['name', 'symbol', 'price', 'newprice', 'changePercent', 'change'];
  }

  getTrend(value: number) {
    let trend = 'trending_flat';
    if (value > 0) {
      trend = 'trending_up';
    } else if (value < 0) {
      trend = 'trending_down';
    }
    return trend;
  }
}

