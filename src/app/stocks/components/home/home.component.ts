import { Component, OnInit, ViewChild} from '@angular/core';
import { StockQueryRequest} from '../../service/stock-query-requests';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  stockData: any;
  error: string;
  loading: boolean;
  curDay: number;
  date: string;
  display: string;

  stockList$ = this.stockRequest.stockList$;
  alerts$ = this.stockRequest.stockAlerts$;

  constructor(private stockRequest: StockQueryRequest, ) { }

  ngOnInit() {
    this.stockList$.subscribe(resp => {
      this.stockData = resp.stocks;
      this.date = resp.stockDate;
      this.curDay = resp.day;
    });

    this.alerts$.subscribe(info => {
      this.error = info.error;
      this.loading = info.isLoading;
      this.display = this.loading ? 'none' : 'block';
    });

    this.loadInitial();
  }

  loadInitial(): void {
    this.stockRequest.fetchStockList();
  }

  generateRandomChange() {
    const newdate = new Date(this.date);
    this.date = moment(newdate).add(1, 'days').format('dddd, MMMM DD, YYYY');
    this.curDay += 1;
    const newStock = this.stockData.map((data) => {
      const rand = [-10, 10][Math.floor(Math.random() * 2)];
      if( !(parseFloat( data.newprice ) < 0 && rand > 0)){
        data.changePercent = data.changePercent + rand;
        data.change =  (data.changePercent / 100 * data.price).toFixed(2);
        data.newprice = (parseFloat(data.price) + parseFloat(data.change)).toFixed(2);
      }
      return data;
    });
    this.stockRequest.updateStockList(newStock, this.curDay, this.date);
  }
}
