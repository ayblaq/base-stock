
import { StockQueryAction, StockQueryActionTypes, transformResponse } from '../actions/stock-query.actions';
import {StockQuery} from '../../interface/stock-query.type';
import * as moment from 'moment';

export const STOCKQUERY_FEATURE_KEY = 'stockQuery';

export interface State {
  error: string;
  isLoading: boolean;
  stockDate: string;
  stockList: StockQuery[];
  day: number;
}

export interface StockQueryPartialState {
  readonly [STOCKQUERY_FEATURE_KEY]: State;
}

export const initialState: State = {
  error: null,
  isLoading: false,
  stockList: [],
  stockDate: moment().format('dddd, MMMM DD, YYYY'),
  day: 1
};

export function stockQueryReducer(
  state: State = initialState,
  action: StockQueryAction
): State {
  switch(action.type) {
    case StockQueryActionTypes.STOCK_FETCHED_ACTION: {
      return {
        ...state,
        error: null,
        isLoading: false,
        stockList: transformResponse(action.stockList)
      };
    }
    case StockQueryActionTypes.STOCK_FETCH_ACTION: {
      return {
        ...state,
        isLoading: true,
        error: null
      };
    }
    case StockQueryActionTypes.STOCK_UPDATE_ACTION: {
      return {
        ...state,
        stockDate: action.stockDate,
        day: action.day,
        stockList: action.stockList
      };
    }
    case StockQueryActionTypes.STOCK_FETCH_ERROR_ACTION: {
      return {
        ...state,
        error: action.error,
        isLoading: false
      };
    }

    default:
      return state;
  }
}

