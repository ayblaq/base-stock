import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { StockQueryPartialState } from '../reducers/stock-query.reducer';
import {StockQueryResponse} from '../../interface/stock-query.type';
import { map } from 'rxjs/operators';
import {
  StockListFetchAction,
  StockQueryActionTypes,
  StockListFetchedAction,
  StockListFetchErrorAction
} from '../actions/stock-query.actions';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin':'*',
  }),
};

@Injectable()
export class StockQueryEffects {
  @Effect() fetchStockQuery$ = this.dataPersistence.fetch(
    StockQueryActionTypes.STOCK_FETCH_ACTION,
    {
      run: (action: StockListFetchAction, state: StockQueryPartialState) => {
        return this.httpClient
          .get("/stocks.php")
          .pipe(
            map(response => new StockListFetchedAction(response as StockQueryResponse[]))
          );
      },

      onError: (action: StockListFetchAction, error) => {
        return new StockListFetchErrorAction(error.error)
      }
    }
  );

  constructor(
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<StockQueryPartialState>
  ) { }

}
