import { Action } from '@ngrx/store';
import {StockQueryResponse, StockQuery} from '../../interface/stock-query.type';
import { map, pick } from 'lodash-es';

export enum StockQueryActionTypes {
  STOCK_FETCH_ACTION = 'STOCK_FETCH_ACTION',
  STOCK_FETCH_ERROR_ACTION = 'STOCK_FETCH_ERROR_ACTION',
  STOCK_FETCHED_ACTION = 'STOCK_FETCHED_ACTION',
  STOCK_UPDATE_ACTION = 'STOCK_UPDATE_ACTION',
}

export class StockListFetchAction implements Action {
  readonly type = StockQueryActionTypes.STOCK_FETCH_ACTION;
  constructor() { }
}

export class StockListUpdateAction implements Action {
  readonly type = StockQueryActionTypes.STOCK_UPDATE_ACTION;
  constructor(public stockList: StockQuery[],public day: number, public stockDate: string) { }
}

export class StockListFetchErrorAction implements Action {
  readonly type = StockQueryActionTypes.STOCK_FETCH_ERROR_ACTION;
  constructor(public error: string) { }
}

export class StockListFetchedAction implements Action {
  readonly type = StockQueryActionTypes.STOCK_FETCHED_ACTION;
  constructor(public stockList: StockQueryResponse[]) { }
}

export type StockQueryAction =
  | StockListFetchAction
  | StockListFetchedAction
  | StockListUpdateAction
  | StockListFetchErrorAction;

export const fromStockeActions = {
  StockListFetchAction,
  StockListFetchedAction,
  StockListUpdateAction,
  StockListFetchErrorAction
};


export function transformResponse(
  response: StockQueryResponse[]
): StockQuery[] {
  return map(
    response,
    responseItem =>
      ({
        ...pick(responseItem, [
          'price',
          'name',
          'symbol',
          'change',
          'changePercent',
        ]),
        change: 0.0,
        changePercent: 0,
        newprice: responseItem.price,
        percent: 100
      } as StockQuery)
  );
}

