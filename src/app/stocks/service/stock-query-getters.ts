import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  STOCKQUERY_FEATURE_KEY
} from '../store/reducers/stock-query.reducer';

const getStockListState = createFeatureSelector<State>(
    STOCKQUERY_FEATURE_KEY
);

export const getStockList = createSelector(
  getStockListState,
  (state: State) => {
     return {stocks: state.stockList, day: state.day, stockDate: state.stockDate};
  }
);

export const getAlerts = createSelector(
  getStockListState,
  (state: State) => {
    return {error: state.error, isLoading: state.isLoading};
  }
);

