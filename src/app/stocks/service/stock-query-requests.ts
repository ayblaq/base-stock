import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { StockQueryPartialState } from '../store/reducers/stock-query.reducer';
import { getAlerts, getStockList } from '../service/stock-query-getters';
import { StockListFetchAction, StockListUpdateAction} from '../store/actions/stock-query.actions';
import { StockQuery } from '../interface/stock-query.type';

@Injectable()
export class StockQueryRequest {
  stockAlerts$ = this.store.pipe(select(getAlerts));
  stockList$ = this.store.pipe(select(getStockList));

  constructor(private store: Store<StockQueryPartialState>) { }

  fetchStockList() {
    this.store.dispatch(new StockListFetchAction());
  }

  updateStockList(stockList: StockQuery[], day: number, stockDate: string) {
    this.store.dispatch(new StockListUpdateAction(stockList, day, stockDate));
  }
}
