# About

A simple random stock generator. 

# BaseStock

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.0 and angular material version 7.3.7. 

## Development server

- Run `npm install` to install needed modules. 
- Run `npm start` for a dev server. Navigate to `http://localhost:4000/`. The app will automatically reload if you change any of the source files.

## Run with ng serve
- Run `npm install` to install needed modules. 
- Run `ng serve --proxy-config proxy.json --port 4000`. This will launch the app in `http://localhost:4000/`